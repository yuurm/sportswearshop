package ru.sportwearcompany.sportswearshop.security;

import lombok.RequiredArgsConstructor;
import org.dom4j.io.SAXContentHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.sportwearcompany.sportswearshop.model.User;
import ru.sportwearcompany.sportswearshop.repo.UserRepository;

@Service
@RequiredArgsConstructor
public class UserPrincipalDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Bean
    public Authentication getAuth(){
        return SecurityContextHolder.getContext().getAuthentication();
    }


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(s);
        return new UserPrincipal(user);
    }
}
