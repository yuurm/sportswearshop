package ru.sportwearcompany.sportswearshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SportswearshopApplication {

    public static void main(String[] args) {
        SpringApplication.run(SportswearshopApplication.class, args);
    }

}
