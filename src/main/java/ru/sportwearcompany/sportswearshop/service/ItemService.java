package ru.sportwearcompany.sportswearshop.service;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.sportwearcompany.sportswearshop.model.CartItem;
import ru.sportwearcompany.sportswearshop.model.Item;
import ru.sportwearcompany.sportswearshop.repo.CartItemRepository;
import ru.sportwearcompany.sportswearshop.repo.ItemImageRepository;
import ru.sportwearcompany.sportswearshop.repo.ItemRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ItemService {
    private final ItemRepository itemRepository;

    private final CartItemRepository cartItemRepository;

    private final ItemImageRepository itemImageRepository;


    public List<Item> getAllItems(){
        return itemRepository.findAll();
    }

    public List<Item> getAllActiveItems(){
        return itemRepository.findAllByActiveTrue();
    }

    public List<Item> getAllItemsByCategory(String category, Long itemId){
        List<Item> allItemsByCategory = itemRepository.findAllByActiveTrueAndCategory(category);
        Item item = itemRepository.findItemById(itemId);
        allItemsByCategory.remove(item);

        return allItemsByCategory;
    }


    public void saveItem(Item item){
        itemRepository.save(item);
    }

    public Item getItemById(Long id){
        return itemRepository.findItemById(id);
    }

    public void saveCartItem(CartItem cartItem){
        cartItemRepository.save(cartItem);
    }

    public CartItem getCartItemById(Long id){
        return cartItemRepository.getById(id);
    }



}
