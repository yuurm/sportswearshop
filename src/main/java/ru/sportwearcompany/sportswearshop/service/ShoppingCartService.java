package ru.sportwearcompany.sportswearshop.service;


import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.sportwearcompany.sportswearshop.model.ShoppingCart;
import ru.sportwearcompany.sportswearshop.model.User;
import ru.sportwearcompany.sportswearshop.repo.ShoppingCartRepository;
import ru.sportwearcompany.sportswearshop.repo.UserRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ShoppingCartService {
    private final ShoppingCartRepository shoppingCartRepository;


   public ShoppingCart getShoppingCartById(Long id){
       return shoppingCartRepository.findShoppingCartById(id);
   }

   public ShoppingCart getActiveShoppingCartByUsername(String username, Boolean isCompleted){
       List<ShoppingCart> shoppingCarts = shoppingCartRepository.findAllByUserUsernameAndCompleted(username, isCompleted);

       if(!shoppingCarts.isEmpty()){
           return shoppingCarts.get(0);
       }

       return new ShoppingCart();
   }

   public List<ShoppingCart> getCompletedShoppingCarts(String username, Boolean isCompleted){
       return shoppingCartRepository.findAllByUserUsernameAndCompletedOrderByDateDesc(username, isCompleted);
   }


   public List<ShoppingCart> getAllShoppingCarts(){
       return shoppingCartRepository.findAll();
   }

   public void saveShoppingCart(ShoppingCart shoppingCart){
       shoppingCartRepository.save(shoppingCart);
   }

   public void deleteShoppingById(Long id){
       shoppingCartRepository.deleteById(id);
   }
}
