package ru.sportwearcompany.sportswearshop.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sportwearcompany.sportswearshop.model.CartItem;

public interface CartItemRepository extends JpaRepository<CartItem, Long> {
   CartItem getById(Long id);

}
