package ru.sportwearcompany.sportswearshop.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sportwearcompany.sportswearshop.model.CartItem;
import ru.sportwearcompany.sportswearshop.model.ItemImage;

public interface ItemImageRepository extends JpaRepository<ItemImage, Long> {
   void deleteAllById(Long id);

}
