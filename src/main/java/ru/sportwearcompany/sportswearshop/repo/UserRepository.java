package ru.sportwearcompany.sportswearshop.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sportwearcompany.sportswearshop.model.User;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {
    @Override
    List<User> findAll();

    User findUserById(Long id);

    User findUserByUsername(String username);

    User findByUsername(String s);

    User findByEmail(String email);

    List<User> findAllByUsernameContainingOrEmailContaining(String  username, String email);
}
